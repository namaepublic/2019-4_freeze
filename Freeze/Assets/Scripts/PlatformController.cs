﻿using NaughtierAttributes;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    [SerializeField, Hide, FindObject] private PlatformSpritesManager _platformSpritesManager;
    [SerializeField, Hide, GetComponent] private Animator _animator;
    [SerializeField] private Transform _platformTransform;
    [SerializeField] private BoxCollider2D _boxCollider2D;
    [SerializeField] private bool _moving;
    [SerializeField] private Vector2Int _tilesSize;
    [SerializeField] private PlatformTile[] _tiles;

#if UNITY_EDITOR
    [Button]
    public void Init()
    {
        DestroyAllChildren();

        var length = _tilesSize.x * _tilesSize.y;
        _tiles = new PlatformTile[length];
        var tileSize = _platformSpritesManager.TileSize;
        var sprites = _platformSpritesManager.GetSprites(_tilesSize);
        for (var y = 0; y < _tilesSize.y; y++)
        for (var x = 0; x < _tilesSize.x; x++)
        {
            var i = x + y * _tilesSize.x;
            _tiles[i] = _platformSpritesManager.GetPrefab()
                .Instantiate((Vector2) transform.position + new Vector2(x * tileSize, -y * tileSize), _platformTransform);
            _tiles[i].SetSprite(sprites[i]);
        }

        _boxCollider2D.offset = new Vector2((_tilesSize.x - 1) * 0.5f, (_tilesSize.y - 1) * -0.5f);
        _boxCollider2D.size = _tilesSize;
    }

    private void DestroyAllChildren()
    {
        for (var i = _platformTransform.childCount; i > 0; --i)
            DestroyImmediate(_platformTransform.GetChild(0).gameObject);
    }
#endif

    public void Freeze(bool freeze)
    {
        if (!_moving) return;
        _animator.enabled = !freeze;
        foreach (var tile in _tiles) tile.SetColor(freeze ? _platformSpritesManager.FrozenColor : Color.white);
    }
}