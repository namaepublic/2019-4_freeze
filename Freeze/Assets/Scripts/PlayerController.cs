﻿using System;
using System.Linq;
using NaughtierAttributes;
using TMPro;
using UnityEngine;

[Flags]
public enum Intent : byte
{
    Left = 0b1,
    Right = 0b10,
    Jump = 0b100,
    Dash = 0b1000
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [Header("Components")] [SerializeField]
    private Transform _graphicsTransform;

    [SerializeField] private SpriteRenderer _staffRenderer;
    [SerializeField] private GameObject _doubleJumpLBL, _dashLBL, _freezeLBL;
    [SerializeField, Hide, GetComponent] private Rigidbody2D _rigidbody2D;
    [SerializeField, Hide, FindObjects] private PlatformController[] _platformControllers;

    [Header("Keys")] [SerializeField] private KeyCode[] _leftKeys;
    [SerializeField] private KeyCode[] _rightKeys;
    [SerializeField] private KeyCode[] _jumpKeys;
    [SerializeField] private KeyCode[] _dashKeys;
    [SerializeField] private KeyCode[] _freezeKeys;

    [Header("Movement stats")] [SerializeField]
    private float _groundedSpeed;

    [SerializeField] private bool _canDash;
    [SerializeField] private float _midAirSpeed;
    [SerializeField] private float _dashSpeed;
    [SerializeField] private float _dashDelay;

    [Header("Jump stats")] [SerializeField]
    private float _jumpForce;

    [SerializeField] private int _jumpAmount;
    [SerializeField] private float _multipleJumpDelay;
    [SerializeField] private float _groundCheckRadius;
    [SerializeField] private Vector2 _groundCheckOffset;
    [SerializeField] private LayerMask _groundLayerMask;

    [Header("Freeze stats")] [SerializeField]
    private bool _canFreeze;

    [SerializeField] private float _freezeDuration;
    [SerializeField] private float _freezeDelay;

    private Intent m_intent;

    private bool m_isGrounded;

    private float m_dashCooldown;

    private int m_jumpCount;
    private float m_multipleJumpCooldown;

    private bool m_frozen;
    private float m_freezeTimer;
    private float m_freezeCooldown;

    public bool IsGrounded => m_isGrounded;

    public void SetJumpAmount(int value) => _jumpAmount = value;

    public bool CanDash
    {
        get => _canDash;
        set => _canDash = value;
    }

    public bool CanFreeze
    {
        get => _canFreeze;
        set => _canFreeze = value;
    }

    public void SetFreezeDuration(float value) => _freezeDuration = value;

    private void Update()
    {
        HandleIntents();
        if (CanFreeze) HandleFreeze();
        HandleTimers(Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (m_isGrounded = CheckGround()) m_jumpCount = _jumpAmount;

        var deltaTime = Time.deltaTime;
        var deltaSpeed = deltaTime * (m_isGrounded ? _groundedSpeed : _midAirSpeed);

        if (HasIntent(Intent.Dash))
        {
            if (m_dashCooldown <= 0)
            {
                m_dashCooldown = _dashDelay;
                deltaSpeed = deltaTime * _dashSpeed;
            }

            RemoveIntent(Intent.Dash);
        }

        if (HasIntent(Intent.Left))
        {
            FlipX(true);
            _rigidbody2D.position += deltaSpeed * Vector2.left;
        }

        if (HasIntent(Intent.Right))
        {
            FlipX(false);
            _rigidbody2D.position += deltaSpeed * Vector2.right;
        }

        if (HasIntent(Intent.Jump) && m_jumpCount > 0 && m_multipleJumpCooldown <= 0)
        {
            m_multipleJumpCooldown = _multipleJumpDelay;
            _rigidbody2D.velocity = _jumpForce * Vector2.up;
            RemoveIntent(Intent.Jump);
            m_jumpCount--;
        }
    }

    private void HandleIntents()
    {
        if (CheckKeysDown(_leftKeys))
        {
            RemoveIntent(Intent.Right);
            AddIntent(Intent.Left);
        }

        if (CheckKeysDown(_rightKeys))
        {
            RemoveIntent(Intent.Left);
            AddIntent(Intent.Right);
        }

        if (CheckKeysUp(_leftKeys))
        {
            RemoveIntent(Intent.Left);
            CheckAndAddIntent(_rightKeys, Intent.Right);
        }

        if (CheckKeysUp(_rightKeys))
        {
            RemoveIntent(Intent.Right);
            CheckAndAddIntent(_leftKeys, Intent.Left);
        }

        if (m_jumpCount == _jumpAmount) CheckAndAddIntent(_jumpKeys, Intent.Jump);
        else if (m_jumpCount > 0) CheckDownAndAddIntent(_jumpKeys, Intent.Jump);

        if (CanDash) CheckDownAndAddIntent(_dashKeys, Intent.Dash);
    }

    private void HandleFreeze()
    {
        if (!m_frozen && CheckKeysDown(_freezeKeys) && m_freezeCooldown <= 0) Freeze();
        if (m_frozen && m_freezeTimer <= 0) UnFreeze();
    }

    private void HandleTimers(float deltaTime)
    {
        m_multipleJumpCooldown = m_isGrounded ? 0 : m_multipleJumpCooldown - deltaTime;
        if (_canDash) m_dashCooldown -= deltaTime;
        if (_canFreeze)
        {
            if (!m_frozen && m_freezeCooldown > 0) m_freezeCooldown -= deltaTime;
            if (m_frozen && m_freezeTimer > 0) m_freezeTimer -= deltaTime;
        }
    }

    public void FlipX() => FlipX(transform.localScale.x > 0);

    public void FlipX(bool faceLeft)
    {
        var scale = _graphicsTransform.localScale;
        scale.x = faceLeft ? -Mathf.Abs(scale.x) : Mathf.Abs(scale.x);
        _graphicsTransform.localScale = scale;
    }

    public void Freeze()
    {
        m_frozen = true;
        m_freezeTimer = _freezeDuration;
        foreach (var platformController in _platformControllers) platformController.Freeze(true);
    }

    public void UnFreeze()
    {
        m_frozen = false;
        m_freezeCooldown = _freezeDelay;
        foreach (var platformController in _platformControllers) platformController.Freeze(false);
    }

    private bool CheckGround() => Physics2D.OverlapCircle(_rigidbody2D.position + _groundCheckOffset, _groundCheckRadius, _groundLayerMask);

    private static bool CheckKeys(KeyCode[] keys) => keys.Any(key => Input.GetKey(key));

    private static bool CheckKeysDown(KeyCode[] keys) => keys.Any(key => Input.GetKeyDown(key));

    private static bool CheckKeysUp(KeyCode[] keys) => keys.Any(key => Input.GetKeyUp(key));

    private bool HasIntent(Intent intent) => (m_intent & intent) != 0;

    private void AddIntent(Intent intent) => m_intent |= intent;

    private void RemoveIntent(Intent intent) => m_intent &= ~intent;

    private void IsolateIntent(Intent intent) => m_intent &= intent;

    private void CheckAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (CheckKeys(keys)) AddIntent(intent);
    }

    private void CheckDownAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (CheckKeysDown(keys)) AddIntent(intent);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Double Jump"))
        {
            _jumpAmount = 2;
            _doubleJumpLBL.SetActive(true);
            other.gameObject.SetActive(false);
        }

        if (other.CompareTag("Dash"))
        {
            _canDash = true;
            _dashLBL.SetActive(true);
            other.gameObject.SetActive(false);
        }

        if (other.CompareTag("Staff"))
        {
            _canFreeze = true;
            _staffRenderer.enabled = true;
            _freezeLBL.SetActive(true);
            other.gameObject.SetActive(false);
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_rigidbody2D.position + _groundCheckOffset, _groundCheckRadius);
    }
#endif
}