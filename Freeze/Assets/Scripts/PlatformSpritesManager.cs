﻿using UnityEngine;

public class PlatformSpritesManager : MonoBehaviour
{
    [SerializeField] private float _tileSize;
    [SerializeField] private Color _frozenColor;

    [SerializeField] private Sprite _topLeftSprite, _topSprite, _topRightSprite;
    [SerializeField] private Sprite _leftSprite, _centerSprite, _rightSprite;
    [SerializeField] private Sprite _bottomLeftSprite, _bottomSprite, _bottomRightSprite;
    [SerializeField] private Sprite _singleLeftSprite, _singleCenterSprite, _singleRightSprite;
    [SerializeField] private Sprite _singleTopSprite, _singleBottomSprite, _singleMiddleSprite;
    [SerializeField] private Sprite _singleSprite;

    [SerializeField] private PlatformTile _platformTilePrefab;

    public PlatformTile GetPrefab() => _platformTilePrefab;

    public float TileSize => _tileSize;

    public Color FrozenColor => _frozenColor;

    public Sprite[] GetSprites(Vector2Int tilesSize)
    {
        if (tilesSize.x == 0 || tilesSize.y == 0) return null;
        if (tilesSize.x == 1 && tilesSize.y == 1) return new[] {_singleSprite};
        int length, i;
        Sprite[] sprites;
        if (tilesSize.x == 1)
        {
            length = tilesSize.y;
            sprites = new Sprite[length];
            sprites[0] = _singleTopSprite;
            sprites[length - 1] = _singleBottomSprite;
            for (i = 1; i < length - 1; i++) sprites[i] = _singleMiddleSprite;
            return sprites;
        }

        if (tilesSize.y == 1)
        {
            length = tilesSize.x;
            sprites = new Sprite[length];
            sprites[0] = _singleLeftSprite;
            sprites[length - 1] = _singleRightSprite;
            for (i = 1; i < length - 1; i++) sprites[i] = _singleCenterSprite;
            return sprites;
        }

        length = tilesSize.x * tilesSize.y;
        sprites = new Sprite[length];
        sprites[0] = _topLeftSprite;
        for (i = 1; i < tilesSize.x - 1; i++) sprites[i] = _topSprite;
        sprites[i] = _topRightSprite;

        for (var j = 1; j < tilesSize.y - 1; j++)
        {
            var line = j * tilesSize.x;
            sprites[line] = _leftSprite;
            for (i = 1; i < tilesSize.x - 1; i++) sprites[i + line] = _centerSprite;
            sprites[i + line] = _rightSprite;
        }

        sprites[length - tilesSize.x] = _bottomLeftSprite;
        for (i = length - tilesSize.x + 1; i < length - 1; i++) sprites[i] = _bottomSprite;
        sprites[i] = _bottomRightSprite;
        
        return sprites;
    }
}