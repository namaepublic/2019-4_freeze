﻿using NaughtierAttributes;
using UnityEngine;

public class PlatformTile : MonoBehaviour
{
    [SerializeField, Hide, GetComponent] private SpriteRenderer _spriteRenderer;

    public void SetSprite(Sprite sprite) => _spriteRenderer.sprite = sprite;
    public void SetColor(Color color) => _spriteRenderer.color = color;

#if UNITY_EDITOR
    public PlatformTile Instantiate(Vector2 position, Transform parent) => Instantiate(this, position, Quaternion.identity, parent);
#endif
}
